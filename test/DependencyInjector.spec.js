import assert from 'assert';
import chai from 'chai';
import DependencyInjector from '../src/DependencyInjector';

const expect = chai.expect;
const should = chai.should;

describe('DependencyInjector.js', () => {
    describe('#constructor()', () => {
        it('should instantiate', () => {
            const di = new DependencyInjector();

            expect(di instanceof DependencyInjector).to.be.true;
        });
    });

    describe('#all()', () => {
        it('should return all registered items', () => {
            const di = new DependencyInjector();

            di.constant('FOO', 'foo');
            di.singleton('singleton', arg => {
                return arg;
            });
            di.register('bar', 'bae');

            const all = di.all();

            expect(all).to.have.property('FOO');
            expect(all).to.have.property('singleton');
            expect(all).to.have.property('bar');
        });
    });

    describe('#constant()', () => {
        it('should set the given value for the given key', () => {
            const di = new DependencyInjector();

            di.constant('CONSTANT', 'foo');

            expect(di.resolve('CONSTANT')).to.equal('foo');
        });

        it('should prevent writing on an existing key/value', () => {
            const di = new DependencyInjector();

            di.constant('CONSTANT', 'foo');
            di.constant('CONSTANT', 'bar');

            expect(di.resolve('CONSTANT')).to.equal('foo');
        });
    });

    describe('#has()', () => {
        it('should return true if the given key exist in dependencies', () => {
            const di = new DependencyInjector();

            di.register('foo', 'bar');

            expect(di.has('foo')).to.be.true;
        });

        it('should return false if the given key not exist in dependencies', () => {
            const di = new DependencyInjector();

            di.register('foo', 'bar');

            expect(di.has('baz')).to.be.false;
        });
    });

    describe('#parameters()', () => {
        it('should return all parameters of the given method', () => {
            const di = new DependencyInjector();
            di.register('FOO', 'foo');
            const func = (arg1, FOO) => {};

            expect(di.parameters(func))
                .to.be.an('array')
                .that.include.members([undefined, 'foo']);
        });

        it('should return all parameters of the given method with dependencies', () => {
            const di = new DependencyInjector();
            const func = (arg1, FOO) => {};

            di.constant('FOO', 'foo');

            expect(di.parameters(func))
                .to.be.an('array')
                .that.include.members([undefined, 'foo']);
        });
    });

    describe('#patch()', () => {
        it('should inject resolved dependencies to the given method', () => {
            const di = new DependencyInjector();

            di.constant('FOO', 'foo');

            const func = di.patch((arg1, FOO) => {
                return [arg1, FOO];
            });

            expect(func('bar'))
                .to.be.an('array')
                .that.include.members(['bar', 'foo']);
        });
    });

    describe('#register()', () => {});

    describe('#resolve()', () => {
        it('should resolve a function and inject resolved dependencies', () => {
            const di = new DependencyInjector();

            di.constant('FOO', 'foo');
            di.register('bar', (FOO) => {
                return FOO;
            });

            expect(di.resolve('bar')()).to.equal('foo');
        });
    });

    describe('#singleton()', () => {
        it('should return a singleton value', () => {
            const di = new DependencyInjector();
            let count = 0;

            di.singleton('foo', () => ++count);

            const foo = di.resolve('foo');

            foo();
            foo();

            expect(count).to.equal(1);
        });
    });

    describe('#unregister()', () => {
        it('should unregister a registered dependencie', () => {
            const di = new DependencyInjector();

            di.register('foo', 'bar');
            di.unregister('foo');

            expect(di.resolve('foo')).to.be.undefined;
        });
    });
});
