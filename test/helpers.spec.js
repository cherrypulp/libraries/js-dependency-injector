import assert from 'assert';
import chai from 'chai';
import { parameters, memoize } from '../src/helpers';

const expect = chai.expect;
const should = chai.should;

describe('helpers.js', () => {
    describe('#parameters', () => {
        it('should return all parameters of the given method', () => {
            const func = (arg1, FOO) => {};

            expect(parameters(func))
                .to.be.an('array')
                .that.include.members(['arg1', 'FOO']);
        });

        it('should return an empty array as parameters of the given method', () => {
            const func = () => {};

            expect(parameters(func))
                .to.be.an('array')
                .that.is.empty;
        });
    });

    describe('#memoize()', () => {
        it('should call the memoize function and return cached result', () => {
            const cache = new Map();
            let count = 0;
            const func = memoize((value) => {
                count++;
                // console.log('->', value, count);
                return value;
            }, cache);

            func('foo');
            func('foo');
            func('foo');

            expect(count).to.equal(1);
        });

        it('should call the given method and return a new result', () => {
            const cache = new Map();
            let count = 0;
            const func = memoize((value) => {
                count++;
                // console.log('->', value, count);
                return value;
            }, cache);

            func('foo');
            func('bar');
            func('baz');

            expect(count).to.equal(3);
        });
    });
});
