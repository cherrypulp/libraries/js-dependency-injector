const path = require('path');
const webpack = require('webpack');

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, 'src'),
        stats: {
            modules: false,
            cached: false,
            colors: true,
            chunk: false,
        },
    },
    devtool: 'source-map',
    entry: path.join(__dirname, 'src', 'DependencyInjector.js'),
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(js)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
        ],
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'DependencyInjector.js',
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(true),
    ],
    resolve: {
        extensions: ['*', '.js'],
    },
    stats: {
        colors: true,
    },
    target:    'web',
};
