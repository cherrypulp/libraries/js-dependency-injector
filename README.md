# DependencyInjector

[![codebeat badge](https://codebeat.co/badges/bd159c8f-84cc-4c70-bf1a-321e370f9e57)](https://codebeat.co/projects/gitlab-com-cherrypulp-libraries-js-dependency-injector-master)
[![Issues](https://img.shields.io/badge/issues-0-brightgreen)](https://gitlab.com/cherrypulp/libraries/js-dependency-injector/issues)
[![License](https://img.shields.io/npm/l/@cherrypulp/dependency-injector)](https://gitlab.com/cherrypulp/libraries/js-dependency-injector/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Fdependency-injector.svg)](https://badge.fury.io/js/%40cherrypulp%2Fdependency-injector)


A basic and simple dependency injector with no dependencies.


## Why use dependency injection?

You might by now be thinking "hey that's pretty cool, but why would I do that?". There's a couple reasons why you might want to use dependency injection in VueJS

### Testability

Imagine you're writing an application that makes calls to an API, you're probably using a package for making those HTTP requests.
However, when you're running your unit tests you'd probably rather not have those tests run wild and create 200 users in your API. Using dependency injection you could just swap out the binding for your HTTP service with a dummy service (that could for example assert the required calls are made), without having to change a single line of code.

Example

```javascript
import axios from 'axios';

deps.singleton('$http', axios);
```

Then for your unit tests

```javascript
deps.singleton('$http', DummyHttpService);
```

### Explicit dependencies

TODO

### Easier refactoring

TODO

## Installation

Installing using `npm install @cherrypulp/dependency-injector`?

**WARNING** Look at [this](#mangling) before building for production.


## Quick start

### Vanilla

```javascript
import DependencyInjector from '@cherrypulp/dependency-injector';

const deps = new DependencyInjector();

deps.constant('foo', 'BAR');
deps.register('bar', (key) => key);
deps.singleton('api', ApiService);

// then

deps.resolve('foo'); // return 'BAR'
deps.resolve('bar')('ok'); // return 'ok'
deps.resolve('api'); // return Api service
```

#### Resolve parameters

```javascript
function func(foo, api) {
    /* ... */
}

const params = deps.parameters(func); // return ['BAR', ApiService]
```

#### Patch method

**WARNING** Look at [this](#mangling) before building for production.

```javascript
const func = deps.patch(function (foo, api) {
    // foo === 'BAR'
    // api === ApiService
});
```

### VueJS

```javascript
import VueDependencyInjector from 'dependency-injector/vue-dependency-injector';

const options = {
    constants: {
        foo: 'BAR',
    },
    factories: {
        bar: (key) => { ... },
    },
    services: {
        api: Api,
    },
};

Vue.use(VueDependencyInjector, options);

// or

new Vue({
    el: '#app',
    ...options,
});

// then

this.$dependency.constant('foo'); // return 'BAR';
this.$dependency.factory('bar')(key); // use bar factory
this.$dependency.service('api'); // return Api service;
```

TODO

## Mangling

When building for production you'll most likely minify your code. Most minifiers employ a technique called "name mangling" which poses a couple problems with this library if you use method injection. You can read more about this here.

In short, you'll need to ensure your minifier won't mangle the parameter names of your services, for UglifyJS (default for webpack) this can be done like this:

```javascript
new webpack.optimize.UglifyJsPlugin({
    compress: {
        warnings: false
    },
    sourceMap: true,
    mangle: {
        except: ['Service'], // blacklist your services from being mangled
    },
});
```


## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/js-dependency-injector/blob/master/LICENSE) file for details.


## TODOS

- [ ] Documentation
- [ ] VueX integration
- [ ] Unit tests
- [x] use Map instead of Object for $$dependencies
- [x] ability to freeze values
- [x] ability to cache values
