import DependencyInjector from './DependencyInjector';

/**
 * Vue Dependency Injector Plugin
 * @example
 * const options = {
 *     constants: {
 *         foo: 'BAR',
 *     },
 *     factories: {
 *         bar: (key) => { ... },
 *     },
 *     services: {
 *         api: Api,
 *     },
 * };
 *
 * Vue.use(VueDependencyInjector, options);
 *
 * // or
 *
 * new Vue({
 *     el: '#app',
 *     ...options,
 * });
 *
 * // then
 *
 * // - get
 * this.$dependency.constant('foo'); // return 'BAR';
 * this.$dependency.factory('bar')(key); // use bar factory
 * this.$dependency.service('api'); // return Api service;
 *
 * // - set
 * this.$dependency.constant('foo', 'BAT); // set 'foo' to 'BAT';
 * this.$dependency.factory('bar', function () {
 *     return null;
 * }); // set bar factory
 * this.$dependency.service('api', ApiMock); // set Api service;
 */
export default {
    name: 'vue-dependency-injector',
    production: process.env.NODE_ENV === 'production',
    install(Vue, options = {}) {
        options = Object.assign({
            constants: {},
            factories: {},
            services: {},
        }, options);

        const instance = new DependencyInjector();
        Vue.prototype.$dependency = instance;
        Vue.$dependency = instance;

        Vue.mixin({
            beforeCreate() {
                ['constants', 'factories', 'services']
                    .forEach((type) => {
                        this.$options[type] = {
                            ...options[type],
                            ...this.$options[type],
                        };

                        Object.entries(this.$options[type])
                            .forEach(([key, value]) => this.$dependency[type](key, value));
                    });
            },
        });
    },
};
