import { memoize, parameters } from './helpers';

/**
 * Dependency Injection
 * @example
 * const di = new DependencyInjector();
 * di.register('foo', 'something');
 * di.register('foo', 'something', {freeze: true});
 */
export default class DependencyInjector {
    constructor() {
        this._dependencies = new Map();
    }

    /**
     * Return all dependencies.
     * @example
     * di.all();
     * @return {Object}
     */
    all() {
        const tmp = {};
        this._dependencies.forEach((v, k) => {
            tmp[k] = v.value;
        });
        return tmp;
    }

    /**
     * Constant helper. Cannot be set more than once.
     * @example
     * di.constant('baz', 'baz');
     * @param {String} name Dependency name
     * @param {*} value
     */
    constant(name, value) {
        this.register(name, value, {freeze: true});
    }

    /**
     * Return true if name exists has a dependency.
     * @example
     * di.has('foo');
     * @param {String} name Dependency name
     * @return {Boolean}
     */
    has(name) {
        return this._dependencies.has(name);
    }

    /**
     * Extracts all dependencies names from given function.
     * @example
     * di.register('foo', Foo);
     * function func(foo) { ... }
     * const params = di.parameters(func); // returns [Foo]
     * @param func
     * @param keys
     * @return {Array}
     * @see https://github.com/tests-always-included/dizzy/blob/master/lib/util.js
     */
    parameters(func, keys = null) {
        const params = parameters(func);
        return params.map((name, index) => {
            if (keys && keys[index] !== undefined) {
                name = keys[index];
            }

            return this.resolve(name) || undefined;
        });
    }

    /**
     * Return a function with resolved parameters.
     * @example
     * const func = di.patch(function (foo) { ... }); // "foo" will be injected from dependencies
     * @example
     * const func = di.patch(function (foo) { ... }, ['foo']); // "foo" will be injected from dependencies
     * @param func
     * @param keys
     * @return {Function}
     */
    patch(func, keys = null) {
        const params = this.parameters(func, keys);
        return (...args) => {
            args = params.map((value, key) => args[key] || value);
            return func.call(func, ...args);
        };
    }

    /**
     * Register a dependency.
     * @example
     * di.register('foo', Foo);
     * di.register('bar', () => 42, {memoize: true}); // = singleton
     * di.register('baz', 'baz', {freeze: true}); // = constant
     * @param {String} name Dependency name
     * @param {*} value
     * @param {Object} options
     */
    register(name, value, options = {}) {
        if (typeof name !== 'string') {
            throw new Error('Expected argument [name] type string');
        }

        const item = {
            freeze: false,
            memoize: false,
            value,
            ...this._dependencies.get(name),
        };

        if (item.freeze) {
            return this;
        }

        options = {
            freeze: false,
            memoize: false,
            ...options,
        };

        if (options.memoize && typeof value === 'function') {
            item.memoize = item.memoize || new Map();
            item.value = memoize(value, item.memoize);
        }

        if (options.freeze) {
            item.freeze = true;
        }

        this._dependencies.set(name, item);

        return this;
    }

    /**
     * Resolve a dependency.
     * @example
     * const foo = di.resolve('foo'); // return Foo
     * @param {String} name Dependency name
     * @return {undefined|*}
     */
    resolve(name) {
        if (typeof name !== 'string') {
            throw new Error('Expected argument [name] type string');
        }

        const item = this._dependencies.get(name);

        if (item) {
            if (typeof item.value === 'function') {
                return this.patch(item.value);
            }

            return item.value;
        }

        return undefined;
    }

    /**
     * Singleton helper.
     * @example
     * di.singleton('bar', () => 42);
     * @param {String} name Dependency name
     * @param {*} value
     */
    singleton(name, value) {
        this.register(name, value, {memoize: true});
    }

    /**
     * Unregister a dependency.
     * @example
     * di.unregister('foo'); // return true
     * di.unregister('nope'); // return false
     * @param {String} name Dependency name
     * @return {Boolean}
     */
    unregister(name) {
        return this._dependencies.delete(name);
    }
}
