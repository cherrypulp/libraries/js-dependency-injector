/**
 * Memoization helper.
 * @param func
 * @param context
 * @param cache
 * @return {Function}
 */
export function memoize(func, cache = new Map(), context = null) {
    return (...args) => {
        const key = JSON.stringify(args);

        if (cache.has(key)) {
            return cache.get(key);
        }

        if (func === null || typeof func === 'number' || typeof func === 'undefined' || typeof func.call === 'undefined') {
            cache.set(key, func);
        } else {
            context = context || func;
            cache.set(key, func.call(context, ...args));
        }

        return cache.get(key);
    };
}

/**
 * Return arguments name of the given method.
 * @example
 * function func(foo) { ... }
 * const params = parameters(func); // returns [Foo]
 * @param func
 * @return {Array}
 * @see https://github.com/tests-always-included/dizzy/blob/master/lib/util.js
 */
export function parameters(func) {
    const extracted = func
        .toString()
        .match(/^(function)?\s*[^(]*\(\s*([^)]*)\)?/m);

    if (extracted === null) {
        return [];
    }

    const es6Mode = extracted[1] === 'function' || extracted[1] === undefined;
    const params = es6Mode ? extracted[2] : extracted[1];

    return params
        .replace(/ /g, '')
        .split(',')
        .filter((dep) => dep !== '');
}
